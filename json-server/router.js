var jsonServer = require("json-server");
var server = jsonServer.create();
const db = require("./db.json");
const middlewares = jsonServer.defaults();
const cors = require("cors");

server.use(
  cors({
    origin: true,
    credentials: true,
    preflightContinue: false,
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  })
);
server.options("*", cors());
server.use(middlewares);

server.get("/trips", function (req, res) {
  if (req.query.keyword) {
    const result = db.trips.filter((trip) => {
      const isTag = trip.tags.find((tag) => tag.includes(req.query.keyword));
      return (
        trip.title.includes(req.query.keyword) ||
        trip.description.includes(req.query.keyword) ||
        isTag
      );
    });
    res.json(result);
  } else res.json(db.trips);
});

server.use(function (req, res, next) {
  if (req.method === "POST") {
    req.body.createdAt = Date.now();
  }
  // Continue to JSON Server router
  next();
});

// Use default router
// server.use(router)
server.listen(9000, function () {
  console.log("JSON Server is running");
});
