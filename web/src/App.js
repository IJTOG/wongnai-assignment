import React from "react";
import "./App.css";
import axios from "axios";
import qs from "query-string";
import { createBrowserHistory } from "history";

function App() {
  const [searchKey, setSarchKey] = React.useState("");
  const [trips, setTrips] = React.useState([]);

  React.useEffect(() => {
    async function fetchData() {
      const { data } = await axios.get("http://localhost:9000/trips");
      setTrips(data);
      let params = qs.parse(window.location.search);
      if (params.keyword !== undefined) {
        setSarchKey(params.keyword);
        submitKeyword(params.keyword);
      }
    }

    fetchData();
  }, []);

  const submitKeyword = async (keyword) => {
    const { data } = await axios.get(
      `http://localhost:9000/trips?keyword=${keyword}`
    );
    setTrips(data);
  };

  const tagClicked = (tag) => {
    let history = createBrowserHistory();
    setSarchKey(tag);
    history.push(`?keyword=${tag}`);
    submitKeyword(tag);
  };

  const handleChange = (e) => {
    setSarchKey(e.target.value);
  };

  const keyPress = (evt) => {
    if (evt.keyCode === 13) {
      let history = createBrowserHistory();
      history.push(`?keyword=${searchKey}`);
      submitKeyword(searchKey);
    }
  };

  return (
    <div className="App">
      <div className="container">
        <div className="row justify-content-md-center">
          <div className="col-md-6 col-xs-12">
            <h1 className="display-3 Title-header mt-5">เที่ยวไหนดี</h1>
            <div className="input-group input-group-lg">
              <input
                value={searchKey}
                type="text"
                className="form-control App-input"
                placeholder="หาที่เที่ยวแล้วไปกัน..."
                aria-label="Username"
                aria-describedby="inputGroup-sizing-sm"
                onChange={handleChange}
                onKeyDown={keyPress}
              />
            </div>
          </div>
        </div>
        <div className="row justify-content-md-center mt-4">
          {trips.map((trip, index) => {
            return (
              <div className="col-md-7 col-xs-12 mt-2" key={index}>
                <div className="card trip-card">
                  <div className="card-body">
                    <div className="row">
                      <div className="text-center col-md-4 col-xs-12">
                        <img
                          className="img-trip"
                          src={trip.photos[0]}
                          alt="cover"
                          width="200"
                          height="310"
                        />
                      </div>
                      <div className="col-md-8 col-xs-12">
                        <div className="card-block px-2">
                          <h4 className="card-title">
                            <a
                              className="Title-trip collapse"
                              id="collapseTitle"
                              href={trip.url}
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              {trip.title}
                            </a>
                          </h4>
                          <p
                            className="card-text collapse"
                            id="collapseDescription"
                            aria-expanded="false"
                          >
                            {trip.description}
                          </p>
                          <a
                            href={trip.url}
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            ...อ่านต่อ
                          </a>
                          <p
                            className="card-text collapse"
                            id="collapseTags"
                            aria-expanded="false"
                          >
                            หมวด:
                            {trip.tags.map((tag, index) => {
                              if (trip.tags.length - 1 === index) {
                                return (
                                  <span key={index}>
                                    {" "}
                                    และ
                                    <span
                                      className="tag"
                                      onClick={() => tagClicked(tag)}
                                    >
                                      {tag}
                                    </span>
                                  </span>
                                );
                              } else
                                return (
                                  <span key={index}>
                                    {" "}
                                    <span
                                      className="tag"
                                      onClick={() => tagClicked(tag)}
                                    >
                                      {tag}
                                    </span>
                                  </span>
                                );
                            })}
                          </p>
                        </div>
                        <div className="text-center col-md-12 mt-top-1">
                          <div className="row">
                            {trip.photos.map((img, index) => {
                              if (index > 0 && index < 4)
                                return (
                                  <div
                                    key={index}
                                    className="text-center col-xs-4"
                                  >
                                    <img
                                      className="img-trip-sub"
                                      src={img}
                                      alt="cover"
                                      width="97"
                                      height="90"
                                    />
                                  </div>
                                );
                              else return null;
                            })}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default App;
