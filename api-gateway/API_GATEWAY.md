# Kong Api Gateway

    https://docs.konghq.com/2.0.x/getting-started

    ผมใช้ Kong สำหรับทำ Api gateway เนื่องจากเป็น Opensource 

# Run Kong on Docker

    ** สร้าง docker network และทำการรัน Kong container

    docker network create kong-net

    docker run -d --name kong-database --network=kong-net -p 5432:5432 -e "POSTGRES_USER=kong" -e "POSTGRES_DB=kong" -e "POSTGRES_PASSWORD=kong" postgres:9.6

    docker run --rm --network=kong-net -e "KONG_DATABASE=postgres" -e "KONG_PG_HOST=kong-database" -e "KONG_PG_PASSWORD=kong" -e "KONG_CASSANDRA_CONTACT_POINTS=kong-database" kong:2.0.0 kong migrations bootstrap

    docker run -d --name kong --network=kong-net -e "KONG_DATABASE=postgres" -e "KONG_PG_HOST=kong-database" -e "KONG_PG_PASSWORD=kong" -e "KONG_CASSANDRA_CONTACT_POINTS=kong-database" -e "KONG_PROXY_ACCESS_LOG=/dev/stdout" -e "KONG_ADMIN_ACCESS_LOG=/dev/stdout" -e "KONG_PROXY_ERROR_LOG=/dev/stderr" -e "KONG_ADMIN_ERROR_LOG=/dev/stderr" -e "KONG_ADMIN_LISTEN=0.0.0.0:8001, 0.0.0.0:8444 ssl" -p 8000:8000 -p 8443:8443 -p 127.0.0.1:8001:8001 -p 127.0.0.1:8444:8444 kong:2.0.0

# Add Trips service

    ** รันคำสั่งเพือเพิ่ม Service ***(ส่วนของ host_url ระบุเป็น ipv4 ของ Host หรือที่ๆ api service ทำงานอยู่)***
    curl -i -X POST --url http://localhost:8001/services/ -d "name=trips-mnm" -d "url=http://[host_url]:9000"

    ** รันคำสั่งเพิ่ม route เข้าสู่ service
    curl -i -X POST --url http://localhost:8001/services/trips-mnm/routes -d "paths[]=/"

    